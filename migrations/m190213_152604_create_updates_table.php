<?php

use yii\db\Migration;

/**
 * Handles the creation of table `updates`.
 */
class m190213_152604_create_updates_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('updates', [
            'id' => $this->primaryKey(),
            'ip_data_id' => $this->integer(),
            'date' => $this->timestamp(),
            'status' => $this->smallInteger(),
            'comment' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('updates');
    }
}
