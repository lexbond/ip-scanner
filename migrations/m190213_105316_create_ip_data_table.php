<?php

use yii\db\Migration;

/**
 * Handles the creation of table `ip_data`.
 */
class m190213_105316_create_ip_data_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('ip_data', [
            'id' => $this->primaryKey(),
            'ip' => $this->string()->unique(),
            'country' => $this->string(),
            'city' => $this->string(),
            'created' => $this->timestamp(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('ip_data');
    }
}
