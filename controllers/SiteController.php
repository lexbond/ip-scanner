<?php

namespace app\controllers;

use app\models\IpData;
use app\models\IpForm;
use app\models\SignupForm;
use app\models\Updates;
use app\models\User;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = new IpData();

        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            if($model->addToDb()) {
                Yii::$app->session->setFlash('result', 'Адрес успешно добавлен');
            } else {
                Yii::$app->session->setFlash('result', 'Ошибка добавления адреса');
            }

            return $this->redirect(Yii::$app->request->referrer);
        }

        $provider = new ActiveDataProvider([
            'query' => Updates::find()->with('ipData')->limit(10),
            'sort'=>[
                'defaultOrder'=>[
                    'date'=>SORT_DESC
                ]
            ]
        ]);

        $ipAddresses = new ActiveDataProvider([
            'query' => IpData::find(),
            'sort'=>[
                'defaultOrder'=>[
                    'created'=>SORT_DESC
                ]
            ]
        ]);


        return $this->render('index', [
            'model'=>$model,
            'provider'=>$provider,
            'ipAddresses'=>$ipAddresses
        ]);
    }

    public function actionUpdate()
    {
        $update = new Updates();
        $update->checkAndUpdateIp();

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionDeleteUpdates()
    {
        Updates::deleteAll();
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
