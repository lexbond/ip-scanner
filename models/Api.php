<?php
/**
 * Created by PhpStorm.
 * User: root
 * Email: exbond@mail.ru
 * Date: 13.02.19
 * Time: 17:27
 */

namespace app\models;


use yii\web\HttpException;

class Api
{
    private $api_url = "https://api.2ip.ua/geo.json?ip=";

    /**
     * @param $ip
     * @return mixed
     * @throws HttpException
     */
    public function getIpInfo($ip)
    {
        if(filter_var($ip, FILTER_VALIDATE_IP)) {
            // Отправляем запрос, получаем данные
            $data = $this->sendCurl($this->api_url . $ip);
            $data = json_decode($data);

            if(!isset($data->country)) {
                return false;
            } return $data;

        } else {
            throw new HttpException(400, "Передан неверный IP-Адрес");
            // Либо можно записать ошибку в лог и продолжить
        }
    }

    /**
     * @param $url
     * @return mixed
     */
    private function sendCurl($url)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        $output = curl_exec($ch);

        curl_close($ch);

        if(!$output) return false;
        return $output;
    }
}