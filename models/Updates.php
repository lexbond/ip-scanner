<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "updates".
 *
 * @property int $id
 * @property int $ip_data_id
 * @property string $date
 * @property int $status
 * @property string $comment
 */
class Updates extends \yii\db\ActiveRecord
{
    private $counter = 0;

    const STATUS_SUCCESS = 1;
    const STATUS_ERROR = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'updates';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ip_data_id', 'status'], 'integer'],
            [['date'], 'safe'],
            [['comment'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ip_data_id' => 'Ip Data ID',
            'date' => 'Дата обновления',
            'status' => 'Статус',
            'comment' => 'Комментарий',
        ];
    }

    public function getIpData()
    {
        return $this->hasOne(IpData::className(), ['id'=>'ip_data_id']);
    }

    /**
     * @return bool
     * @throws \yii\web\HttpException
     */
    public function checkAndUpdateIp()
    {
        $ips = IpData::find()->all();
        if (!$ips) return false;

        foreach ($ips as $item) {

            $this->isNewRecord = true;
            $this->id = null;
            $this->date = time();
            $this->ip_data_id = $item->id;
            $this->comment = "";

            $api = new Api();

            if($check = $api->getIpInfo($item->ip)) {

                $this->status = self::STATUS_SUCCESS;
                $this->compareData($item, $check);
                $this->save();

            } else {

                $this->status = self::STATUS_ERROR;
                $this->comment = "Проверьте доступность сервиса API";
            }

            $this->save();
        }
    }

    /**
     * @param $old
     * @param $new
     */
    private function compareData(&$old, $new)
    {
        if($old->country!==$new->country_rus) {
            $this->comment .= "Изменена страна. ";
            $old->country = $new->country_rus;
        }
        if($old->city!==$new->city_rus) {
            $this->comment .= "Изменен город. ";
            $old->city = $new->city_rus;
        }
        if($this->comment!=='') {
            $this->counter++;
            $old->save();
        } else {
            $this->comment = "Изменений нет";
        }
    }

    /**
     * @return int
     */
    public function getCounter()
    {
        return $this->counter;
    }

    /**
     * @param $status
     * @return string
     */
    public static function getStatusLabel($status)
    {
        switch ($status) {
            case self::STATUS_SUCCESS :
                return "Успешно";
            case self::STATUS_ERROR :
                return "Ошибка";
        }
    }
}
