<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ip_data".
 *
 * @property int $id
 * @property string $ip
 * @property string $country
 * @property string $city
 * @property string $created
 */
class IpData extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ip_data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created'], 'safe'],
            [['ip', 'country', 'city'], 'string', 'max' => 255],
            [['ip'], 'unique', 'message' => 'Данный адрес уже присутствует в БД'],
            [['ip'], 'required', 'skipOnEmpty' => false],
            [['ip'], 'ip'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ip' => 'IP-Адрес',
            'country' => 'Страна',
            'city' => 'Город',
            'created' => 'Создано',
        ];
    }

    /**
     * @return bool
     * @throws \yii\web\HttpException
     * Вносим запись об Ip
     */
    public function addToDb()
    {
        $this->created = time();

        if($this->save()) {
            
            // Ищем данные страны и города
            $api = new Api();
            if($data = $api->getIpInfo($this->ip)) {
                $this->country = $data->country_rus;
                $this->city = $data->city_rus;
                $this->save();

                //Внесём данные о проверке
                $update = new Updates();
                $update->ip_data_id = $this->id;
                $update->status = Updates::STATUS_SUCCESS;
                $update->date = time();
                $update->comment = "Добавление нового IP-Адреса";
                $update->save();
            }
            return true;
        } else {
            return false;
        }
    }
}
