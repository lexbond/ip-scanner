<?php

/* @var $this yii\web\View */

use app\models\Updates;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'Ip-scanner';
?>
<script>
    $(document).ready(function () {
        $("#self-ip").on("click", function () {
            $("#ipdata-ip").val($(this).data("ip"));
        })
    });
</script>
<div class="site-index">


        <?php $form = ActiveForm::begin();?>
        <?=$form->field($model, 'ip')->textInput(['placeholder'=>'Введите IP-Адрес'])->label(false)?>

        <div class="form-group">
            <a class="btn btn-default" id="self-ip" data-ip="<?=$_SERVER['REMOTE_ADDR']?>">Вставить мой IP</a>
            <?= Html::submitButton('Добавить адрес', ['class'=>'btn btn-success'])?>
        </div>

        <?php ActiveForm::end()?>

        <?php if(Yii::$app->session->hasFlash('result')):?>
            <div class="alert alert-info text-center">
                <?=Yii::$app->session->getFlash('result')?>
            </div>
        <?php endif;?>
    

    <h3>Ip-Адреса в БД</h3>
    <table class="table table-striped">
        <tr>
            <th>IP</th>
            <th>Страна</th>
            <th>Город</th>
            <th>Дата добавления</th>
        </tr>
        <?php foreach ($ipAddresses->getModels() as $address):?>
            <tr>
                <td><?=$address->ip?></td>
                <td><?=$address->country?></td>
                <td><?=$address->city?></td>
                <td><?=date("d.m.Y H:i:s", $address->created)?></td>
            </tr>
        <?php endforeach;?>
    </table><br><br>


    <h3>Журнал обновления БД</h3>
    <a href="<?= Url::to(['update'])?>" class="btn btn-success">Обновить данные</a>
    <a href="<?= Url::to(['delete-updates'])?>" class="btn btn-danger">Очистить список</a>
    <br><br>
    <table class="table table-striped">
        <tr>
            <th>IP</th>
            <th>Дата обновления</th>
            <th>Статус</th>
            <th>Комментарий</th>
        </tr>
        <?php foreach ($provider->getModels() as $item):?>
            <tr>
                <td><?=$item->ipData->ip?></td>
                <td><?=date("d.m.Y H:i:s", $item->date)?></td>
                <td><?= Updates::getStatusLabel($item->status)?></td>
                <td><?=$item->comment?></td>
            </tr>
        <?php endforeach;?>
    </table>



</div>
