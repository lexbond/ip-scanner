#!/bin/bash

# Установим зависимости
composer install

# Расставим нужные права
chmod -R 777 assets
chmod -R 777 runtime
chmod -R 777 web/assets
chmod -R 777 data

# Установим sqlite если его ещё нет.
# Если на сервере используется php версии выше 7.0, тогда нужно установить пакет этой версии (например: php7.2-sqlite3)
apt-get install php7.0-sqlite3