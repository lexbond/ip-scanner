<?php
/**
 * Created by PhpStorm.
 * User: root
 * Email: exbond@mail.ru
 * Date: 13.02.19
 * Time: 18:27
 */

namespace app\commands;


use app\models\Updates;
use yii\console\Controller;

class ServiceController extends Controller
{
    public $params; // Можем получать доп.параметры через ключ -p


    public function options($actionID)
    {
        return ['params'];
    }

    public function optionAliases()
    {
        return ['p'=>'params'];
    }

    public function actionIndex()
    {
        $update = new Updates();
        $update->checkAndUpdateIp();
        
        echo "Updated records: {$update->getCounter()} \n";
    }

}